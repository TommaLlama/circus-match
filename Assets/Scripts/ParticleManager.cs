﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    //All of these names and values come from the particles we have created and named as such
    //If we want to adjust this later, we can take some more generic parameters

    public GameObject clearFXPrefab;
    public GameObject breakFXPrefab;
    public GameObject doubleBreakFXPrefab;

    public void ClearPieceFXAt(int x, int y, int z = 0)
    {
        if(clearFXPrefab != null)
        {
            GameObject clearFX = Instantiate(clearFXPrefab, new Vector3(x,y,z), Quaternion.identity) as GameObject;
            ParticlePlayer particlePlayer = clearFX.GetComponent<ParticlePlayer>();

            if(particlePlayer != null)
            particlePlayer.Play();
        }
    }

    public void BreakTileFXAt(int breakableVaule, int x, int y, int z = 0)
    {
        GameObject breakFX = null;
        ParticlePlayer particlePlayer = null;

        if(breakableVaule > 1)
        {
            if(doubleBreakFXPrefab != null)
            {
                breakFX = Instantiate(doubleBreakFXPrefab, new Vector3(x, y, z), Quaternion.identity) as GameObject;
            }
        }

        else
        {
            if(breakFX != null)
            {
                breakFX = Instantiate(breakFXPrefab, new Vector3(x, y, z), Quaternion.identity) as GameObject;
            }
        }

        if(breakFX != null)
        {
            particlePlayer = breakFX.GetComponent<ParticlePlayer>();
            if(particlePlayer != null)
            {
                particlePlayer.Play();
            }
        }
    }
}
