using UnityEngine;
using System.Collections;

public enum TileType
{
	Normal,
	Obstacle,
	Breakable
};

[RequireComponent(typeof(SpriteRenderer))]
public class Tile : MonoBehaviour {

	//Let's set our collider to 0.5 so only a large portion of the tile is selectable which covers the dot perfectly  
	
	public int xIndex;
	public int yIndex;
	
	private Board board;

	public TileType tileType = TileType.Normal;

	SpriteRenderer spriteRenderer;

	//Essentially it's health
	public int breakableValue = 0;
	public Sprite[] breakableSprites;

	//Set our spriter renderers color when we break the tile
	public Color normalColor;
	
	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	void Update()
	{
		
	}
	
	public void Init(int x, int y, Board board)
	{
		xIndex = x;
		yIndex = y;
		this.board = board;

		if(tileType == TileType.Breakable)
		{
			if(breakableSprites[breakableValue] != null)
			{
				spriteRenderer.sprite = breakableSprites[breakableValue];
			}
		}

	}
	
	void OnMouseDown()
	{
		if(board != null)
		{
			board.ClickTile(this);
		}
	}
	
	void OnMouseEnter()
	{
		if(board != null)
		{
			board.DragToTile(this);
		}
	}
	
	void OnMouseUp()
	{
		if(board != null)
		{
			board.ReleaseTile();
		}
	}

	public void BreakTile()
	{
		if(tileType != TileType.Breakable)
		{
			return;
		}

		StartCoroutine(BreakTileRoutine());
	}

	IEnumerator BreakTileRoutine()
    {
        breakableValue--;
        breakableValue = Mathf.Clamp(breakableValue, 0, breakableValue);
 
        yield return new WaitForSeconds(0.25f);
        if (breakableSprites[breakableValue] != null)
        {
            spriteRenderer.sprite = breakableSprites[breakableValue];
        }
 
        if (breakableValue <= 0)
        {
            tileType = TileType.Normal;
            spriteRenderer.color = normalColor;
        }
    }  
	
}