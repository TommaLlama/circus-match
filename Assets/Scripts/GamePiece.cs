using UnityEngine;
using System.Collections;

public class GamePiece : MonoBehaviour {
	
	public int xIndex;
	public int yIndex;
	
	Board board;
	
	bool isMoving = false;
	
	//How smooth the object should move
	public enum InterpolationType
	{
		Linear,
		EaseOut,
		EaseIn,
		SmoothStep,
		SmootherStep
	};

	public MatchValue matchValue;

	public enum MatchValue
	{
		Yellow,
		Blue,
		Magenta,
		Indigo,
		Green,
		Teal,
		Red,
		Cyan,
		Wild
	};
	
	public InterpolationType interpolation = InterpolationType.Linear;
	
	void Start()
	{
		
	}
	
	void Update()
	{
		
	}
	
	public void Init(Board board)
	{
		this.board = board;
	}
	
	//Decides what our x and y position are
	public void SetCoord(int x, int y)
	{
		xIndex = x;
		yIndex = y;
		
		transform.position = new Vector3(x,y,0);
	}
	
	public void Move(int destX, int destY, float timeToMove)
	{
		if(!isMoving)
		{
			StartCoroutine(MoveRoutine(new Vector3(destX, destY, 0), timeToMove));
		}
	}
	
	IEnumerator MoveRoutine(Vector3 destination, float timeToMove)
	{
		Vector3 startPosition = transform.position;
		bool reachedDestination = false;
		
		float elapsedTime = 0f;
		isMoving = true;
		
		while(!reachedDestination)
		{
			if(Vector3.Distance(transform.position, destination) < 0.01f)
			{
				reachedDestination = true;
				if(board != null)
				{
					board.PlaceGamePiece(this, (int)destination.x, (int)destination.y);
				}
				break;
			}
			
			elapsedTime += Time.deltaTime;
			
			//Vector3.Lerp has a clamp built in - no need to clamp
			float t = elapsedTime / timeToMove;
			
			switch (interpolation)
			{
				case InterpolationType.Linear:
					break;
					
				case InterpolationType.EaseOut:
					t = Mathf.Sin(t * Mathf.PI * 0.5f);
					break;
				
				case InterpolationType.EaseIn:
					t = 1 - Mathf.Cos(t * Mathf.PI * 0.5f);
					break;
					
				case InterpolationType.SmoothStep:
					t = t * t *(3 - 2 * t);
					break;
					
				case InterpolationType.SmootherStep:
					t = t * t * t * (t* (t * 6 - 15) + 10);
					break;
			}
			
			transform.position = Vector3.Lerp(startPosition, destination, t);
			
			yield return null;
		}
		
		isMoving = false;
		
	}
}