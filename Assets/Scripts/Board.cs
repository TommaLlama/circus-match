using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Board : MonoBehaviour {
	
	public int width;
	public int height;
	
	public int borderSize;
	
	public GameObject tileNormalPrefab;
	public GameObject tileObstaclePrefab;
	public GameObject[] gamePiecePrefabs;

	public GameObject adjacentBombPrefab;
	public GameObject columnBombPrefab;
	public GameObject rowBombPrefab;

	GameObject clickedTileBomb;
	GameObject targetTileBomb;
	
	private Tile[,] allTiles;		//Using an array because the amount of tiles won't change, just the amount of pieces
	private GamePiece[,] allGamePieces;
	
	public float swapTime = 0.25f;
	private Tile clickedTile;
	private Tile targetTile;
	private bool playerInputEnabled = true;

	ParticleManager particleManager;

	public StartingObject[] startingTiles;

	[Tooltip("Allows us to manually decide the positions of game pieces if we want")]
	public StartingObject[] startingGamePieces;

	public int fillYOffset = 10;
	public float fillMoveTime = 0.5f;

	//Not Tiles! They hold references to tiles!
	[System.Serializable]
	public class StartingObject
	{
		public GameObject prefab;
		public int x;
		public int y;
		public int z;
	}
	
	void Start()
	{
		allTiles = new Tile[width,height];
		allGamePieces = new GamePiece[width,height];
		
		SetupTiles();
		SetupGamePieces();
		SetupCamera();

		//Adding a yoffset to make it look prettier when it drops into place
		FillBoard(fillYOffset, fillMoveTime);
		particleManager = GameObject.FindWithTag("ParticleManager").GetComponent<ParticleManager>();
	}
	
	void Update()
	{
		
	}
	
	//Sets up the board with random tiles
	void SetupTiles()
	{
		foreach(StartingObject stile in startingTiles)
		{
			if(stile != null)
			{
				MakeTile(stile.prefab, stile.x, stile.y, stile.z);
			}
		}

		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
            {
				if(allTiles[i,j] == null)
                MakeTile(tileNormalPrefab,i, j);
            }
        }
	}

	//Makes a tile and can be called to create regular game tiles OR obstacles
    private void MakeTile(GameObject prefab, int x, int y, int z = 0)
    {
		if(prefab != null && IsWithinBounds(x,y))
		{
			GameObject tile = Instantiate(prefab, new Vector3(x, y, z), Quaternion.identity) as GameObject;
        	tile.name = "Tile (" + x + "," + y + ")";
        	allTiles[x, y] = tile.GetComponent<Tile>();
     		tile.transform.parent = transform;
        	allTiles[x, y].Init(x, y, this);
		}
        
    }

	void MakeGamePiece(GameObject prefab, int x, int y, int falseYOffset = 0, float moveTime = 0.1f)
	{
		if (prefab != null && IsWithinBounds(x,y))
        {
            prefab.GetComponent<GamePiece>().Init(this);
            PlaceGamePiece(prefab.GetComponent<GamePiece>(), x, y);

			if(falseYOffset != 0)
			{
				prefab.transform.position = new Vector3(x, y + falseYOffset, 0);
				prefab.GetComponent<GamePiece>().Move(x,y,moveTime);
			}

            prefab.transform.parent = transform;
        }
	}

	GameObject MakeBomb(GameObject prefab, int x, int y)
	{
		if(prefab != null && IsWithinBounds(x,y))
		{
			GameObject bomb = Instantiate(prefab, new Vector3(x,y,0), Quaternion.identity) as GameObject;
			bomb.GetComponent<Bomb>().Init(this);
			bomb.GetComponent<Bomb>().SetCoord(x,y);
			bomb.transform.parent = transform;
			return bomb;
		}

		return null;
	}


    //Returns a random gamepiece as a gameobject instead of a gamepiece object so it may be used elsewhere if need be
    GameObject GetRandomPiece()
	{
		int randomIndex = Random.Range(0, gamePiecePrefabs.Length);
		
		if(gamePiecePrefabs[randomIndex] == null)
		{
			Debug.LogWarning("BOARD: " + randomIndex + "does not contain a valid GamePiece prefab!");
		}
		
		return gamePiecePrefabs[randomIndex];
	}
	
	//Fill Random is called then this method places the game piece provided by FillRandom
	public void PlaceGamePiece(GamePiece gamePiece, int x, int y)
	{
		if(gamePiece == null)
		{
			Debug.LogWarning("BOARD: Invalid GamePiece!");
			return;
		}
		
		gamePiece.transform.position = new Vector3(x,y,0);
		transform.rotation = Quaternion.identity;
		if(IsWithinBounds(x,y))
		{
			allGamePieces[x,y] = gamePiece;
		}
		gamePiece.SetCoord(x,y);
	}
	
	bool IsWithinBounds(int x, int y)
	{
		return (x >= 0 && x < width && y >= 0 && y < height);
	}
	
	//Fill the board with random gamepieces, starting from the bottom left(0,0)
	void FillBoard(int falseYOffset = 0, float moveTime = 0.25f)
	{
		int maxIterations = 100;
		int iterations = 0;
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
            {
				//Spawn a gamepiece unless it's an obstacle tile
				if(allGamePieces[i,j] == null && allTiles[i,j].tileType != TileType.Obstacle)
				{
					GamePiece piece = FillRandomAt(i,j, falseYOffset, moveTime);
					while(HasMatchOnFill(i,j))
					{
						ClearPieceAt(i,j);
						piece = FillRandomAt(i,j,falseYOffset, moveTime);
						iterations++;

						if(iterations >= maxIterations)
						{
							Debug.Log("Couldn't go without making a match! Breaking...");
							break;
						}
					}
				}
            }
        }
	}

    private GamePiece FillRandomAt(int x, int y, int falseYOffset = 0, float moveTime = 0.01f)
    {
		if(IsWithinBounds(x,y))
		{
			GameObject randomPiece = Instantiate(GetRandomPiece(), Vector3.zero, Quaternion.identity) as GameObject;
			MakeGamePiece(randomPiece, x, y, falseYOffset, moveTime);
			return randomPiece.GetComponent<GamePiece>();
		}
		
		return null;
    }

	bool HasMatchOnFill(int x, int y, int minLength = 3)
	{
		List<GamePiece> leftMatches = FindMatches(x,y, new Vector2(-1,0), minLength);
		List<GamePiece> downMatches = FindMatches(x,y, new Vector2(0,-1), minLength);

		if(leftMatches == null)
		{
			leftMatches = new List<GamePiece>();
		}

		if(downMatches == null)
		{
			downMatches = new List<GamePiece>();
		}

		return (leftMatches.Count > 0 || downMatches.Count > 0);
	}

	//This allows us to create game pieces at the beginning if we want to alter the board with special pieces or tiles
	//For example, we can set three starting pieces in the inspector, then decide what those pieces are to guarantee matches
	void SetupGamePieces()
	{
		foreach(StartingObject sPiece in startingGamePieces)
		{
			if(sPiece != null)
			{
				GameObject piece = Instantiate(sPiece.prefab, new Vector3(sPiece.x, sPiece.y, 0), Quaternion.identity) as GameObject;
				MakeGamePiece(piece,sPiece.x, sPiece.y, fillYOffset, fillMoveTime);
			}
		}
	}

    //Uses Camera.main, this is bad practice. Send an event instead and have the camera read that event
    void SetupCamera()
	{
		Camera.main.transform.position = new Vector3((float)(width - 1) / 2, (float)(height - 1) / 2, -10f);

		float aspectRatio = (float)Screen.width / (float)Screen.height;

		float verticalSize = (float)height / 2 + (float)borderSize;

		float horizontalSize = ((float)width / 2f + (float) borderSize) / aspectRatio;

		Camera.main.orthographicSize = (verticalSize > horizontalSize) ? verticalSize: horizontalSize;
	}
	
	//When we click a tile, let's see if we already have a clicked tile
	public void ClickTile(Tile tile)
	{
		if(clickedTile == null)
		{
			clickedTile = tile;
		}
	}
	
	//If we have a currently selected tile, then the next tile is where we want the selected tile to go
	public void DragToTile(Tile tile)
	{
		if(clickedTile != null && IsNextTo(tile, clickedTile))
		{
			targetTile = tile;
		}
	}
	
	public void ReleaseTile()
	{
		if(clickedTile != null && targetTile != null)
		{
			SwitchTiles(clickedTile, targetTile);
		}
		
		clickedTile = null;
		targetTile = null;
	}
	
	public void SwitchTiles(Tile clickedTile, Tile targetTile)
	{
		StartCoroutine(SwitchTilesRoutine(clickedTile, targetTile));
	}

	IEnumerator SwitchTilesRoutine(Tile clickedTile, Tile targetTile)
	{
		if(playerInputEnabled)
		{
			//Grab the current tiles x and y position for the clickedPiece and targetPiece
			GamePiece clickedPiece = allGamePieces[clickedTile.xIndex, clickedTile.yIndex];
			GamePiece targetPiece = allGamePieces[targetTile.xIndex, targetTile.yIndex];

			if(targetPiece != null && clickedPiece != null)
			{
				//Swap their positions with their x and y indicies
				clickedPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);
				targetPiece.Move(clickedTile.xIndex, clickedTile.yIndex, swapTime);

				//We'll move the tiles to the new positions and try to make a match
				yield return new WaitForSeconds(0.5f);

				//After swapping, the clicked piece that swapped could make matches, and so can the piece we swapped with
				List<GamePiece> clickedPieceMatches = FindMatchesAt(clickedTile.xIndex, clickedTile.yIndex);
				List<GamePiece> targetPieceMatches = FindMatchesAt(targetTile.xIndex, targetTile.yIndex);

				//If we can't make a match, we'll move back
				if(targetPieceMatches.Count == 0 && clickedPieceMatches.Count == 0)
				{
					//Let's wait before switching back it looks nicer
					yield return new WaitForSeconds(0.5f);
					clickedPiece.Move(clickedTile.xIndex, clickedTile.yIndex,swapTime);
					targetPiece.Move(targetTile.xIndex, targetTile.yIndex,swapTime);
				}
				else
				{
					//Let's clear after waiting just a second
					yield return new WaitForSeconds(0.1f);
					Vector2 swipeDirection = new Vector2(targetTile.xIndex - clickedTile.xIndex, targetTile.yIndex - clickedTile.yIndex);
					clickedTileBomb = DropBomb(clickedTile.xIndex, clickedTile.yIndex, swipeDirection, clickedPieceMatches);
					targetTileBomb = DropBomb(targetTile.xIndex, targetTile.yIndex, swipeDirection, targetPieceMatches);

					ClearAndRefillBoard(clickedPieceMatches.Union(targetPieceMatches).ToList());
				}

			}
		}

	}
	
	//Checks if the start tile and the end tile are within one unit of each other
	//We check if the x value is the same but y is different by 1, or if the y value is the same and the x value is different by 1
	bool IsNextTo(Tile start, Tile end)
	{
		if(Mathf.Abs(start.xIndex - end.xIndex) == 1 && start.yIndex == end.yIndex)
		{
			return true;
		}
		
		if(Mathf.Abs(start.yIndex - end.yIndex) == 1 && start.xIndex == end.xIndex)
		{
			return true;
		}
		
		return false;
	}

	List<GamePiece> FindMatches(int startX, int startY, Vector2 searchDirection, int minLength = 3)
	{
		List<GamePiece> matches = new List<GamePiece>();
		GamePiece startPiece = null;

		//Check if our search starts within bounds
		if(IsWithinBounds(startX,startY))
		{
			startPiece = allGamePieces[startX,startY];
		}

		//If we have a starting piece then add it to our matches so we know what matches to look for
		//Otherwise we didn't find any matches because there is no piece
		if(startPiece != null)
		{
			matches.Add(startPiece);
		}

		else
		{
			return null;
		}

		int nextX;
		int nextY;

		int maxValue = (width > height) ? width: height;

		for(int i = 1; i < maxValue - 1; i++)
		{
			//Finds our current x value in our position, then increments using our search direction to locate a match
			//Same for y
			nextX = startX + (int) Mathf.Clamp(searchDirection.x,-1,1) * i;
			nextY = startY + (int) Mathf.Clamp(searchDirection.y,-1,1) * i;

			if(!IsWithinBounds(nextX,nextY))
			{
				break;
			}

			//The piece we're evaluating against our match value by incrementing
			GamePiece nextPiece = allGamePieces[nextX,nextY];

			//Is there a piece or is it an hole/obstacle? 
			if(nextPiece == null)
			{
				break;
			}
			
			else
			{
				if(nextPiece.matchValue == startPiece.matchValue && !matches.Contains(nextPiece))
				{	
					matches.Add(nextPiece);
				}
				else
				{
					break;
				}
			}
		}

		if(matches.Count >= minLength)
		{
			return matches;
		}

		//No matches? Then we return null
		return null;
	}

	List<GamePiece> FindVerticalMatches(int startX, int startY, int minLength = 3)
	{
		Vector2 upwardsVector = new Vector2(0,1);
		Vector2 downwardsVector = new Vector2(0,-1);
		//Although we want to have a min length of 3 normally, we pass in "2" because we could have overlapping matches
		List<GamePiece> updwardsMatches = FindMatches(startX, startY, upwardsVector, 2);
		List<GamePiece> downwardsMatches = FindMatches(startX, startY, downwardsVector, 2);

		//If either of these are null, we need to make them empty instead of null or Linq.Union will not work
		if(updwardsMatches == null)
		{
			updwardsMatches = new List<GamePiece>();
		}

		if(downwardsMatches == null)
		{
			downwardsMatches = new List<GamePiece>();
		}

		//Adds both lists together without overlapping any exisiting elements, IE matches in this case
		var combinedMatches = updwardsMatches.Union(downwardsMatches).ToList();

		return (combinedMatches.Count >= minLength) ? combinedMatches : null;
	}

	List<GamePiece> FindHorizontalMatches(int startX, int startY, int minLength = 3)
	{
		Vector2 rightVector = new Vector2(1,0);
		Vector2 leftVector = new Vector2(-1,0);
		//Although we want to have a min length of 3 normally, we pass in "2" because we could have overlapping matches
		List<GamePiece> rightMatches = FindMatches(startX, startY, rightVector, 2);
		List<GamePiece> leftMatches = FindMatches(startX, startY, leftVector, 2);

		//If either of these are null, we need to make them empty instead of null or Linq.Union will not work
		if(rightMatches == null)
		{
			rightMatches = new List<GamePiece>();
		}

		if(leftMatches == null)
		{
			leftMatches = new List<GamePiece>();
		}

		//Adds both lists together without overlapping any exisiting elements, IE matches in this case
		var combinedMatches = rightMatches.Union(leftMatches).ToList();

		return (combinedMatches.Count >= minLength) ? combinedMatches : null;
	}

	//This method is called when looking for Gamepieces using the gamepieces x and y that are sent in
	//Then we call find Vert and Horiz matches and highlight them after switching tiles
	List<GamePiece> FindMatchesAt(int x, int y, int minLength = 3)
    {
        List<GamePiece> horizMatches = FindHorizontalMatches(x, y, minLength);
        List<GamePiece> vertMatches = FindVerticalMatches(x, y, minLength);

        if (horizMatches == null)
        {
            horizMatches = new List<GamePiece>();
        }

        if (vertMatches == null)
        {
            vertMatches = new List<GamePiece>();
        }

        var combinedMatches = horizMatches.Union(vertMatches).ToList();
        return combinedMatches;
    }

	List<GamePiece> FindMatchesAt(List<GamePiece> gamePieces, int minLength = 3)
    {
        List<GamePiece> matches = new List<GamePiece>();
		foreach(GamePiece piece in gamePieces)
		{
			matches = matches.Union(FindMatchesAt(piece.xIndex, piece.yIndex, minLength)).ToList();
		}

        return matches;
    }

	List<GamePiece> FindAllMatches()
	{
		List<GamePiece> combinedMatches = new List<GamePiece>();

		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				List<GamePiece> matches = FindMatchesAt(i,j);
				combinedMatches = combinedMatches.Union(matches).ToList();
			}
		}
		return combinedMatches;
	}

	void HighlightTileOff(int x, int y)
	{
		if(allTiles[x,y].tileType != TileType.Breakable)
		{
			//Getting all of our matching sprites and setting there color to transparent
        	SpriteRenderer spriteRenderer = allTiles[x, y].GetComponent<SpriteRenderer>();
			spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 0);
		}
		
	}

	void HighlightTileOn(int x, int y, Color color)
	{
		SpriteRenderer spriteRenderer = allTiles[x, y].GetComponent<SpriteRenderer>();
		spriteRenderer.color = color;
	}

	void HighlightMatchesAt(int x, int y)
    {
        HighlightTileOff(x, y);
        var combinedMatches = FindMatchesAt(x, y);

        if (combinedMatches.Count > 0)
        {
            foreach (GamePiece piece in combinedMatches)
            {
                HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);
            }
        }
    }
	void HighlightMatches()
	{
		for(int	i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
            {
                HighlightMatchesAt(i, j);

            }
        }
	}

	void HighLightPieces(List<GamePiece> gamePieces)
	{
		foreach(GamePiece piece in gamePieces)
		{
			if(piece != null)
			{
				HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);
			}
		}
	}

	void ClearPieceAt(int x, int y)
	{
		GamePiece pieceToClear = allGamePieces[x,y];
		if(pieceToClear != null)
		{
			allGamePieces[x,y] = null;
			Destroy(pieceToClear.gameObject);
		}

		HighlightTileOff(x,y);
	}

	void ClearBoard()
	{
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				ClearPieceAt(i,j);
			}
		}
	}

	//Overload ClearPieceAt
	void ClearPieceAt(List<GamePiece> gamepieces)
	{
		foreach(GamePiece piece in gamepieces)
		{
			if(piece != null)
			{
				ClearPieceAt(piece.xIndex, piece.yIndex);

				if(particleManager != null)
				{
					particleManager.ClearPieceFXAt(piece.xIndex, piece.yIndex);
				}
			}
		}
	}

	void BreakTileAt(int x, int y)
	{
		Tile tileToBreak = allTiles[x,y];
		if(tileToBreak != null && tileToBreak.tileType == TileType.Breakable)
		{
			if(particleManager != null)
			{
				particleManager.BreakTileFXAt(tileToBreak.breakableValue, x, y);
			}
			
			tileToBreak.BreakTile();
		}
	}

	void BreakTileAt(List<GamePiece> gamePieces)
	{
		foreach(GamePiece piece in gamePieces)
		{
			if(piece != null)
			{
				BreakTileAt(piece.xIndex, piece.yIndex);
			}
		}
	}

	List<GamePiece> CollapseColumn(int column, float collapseTime = 0.1f)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();
		 for(int i = 0; i < height -1; i++)
		 {
			 //Don't fall into an obstacle space
			 if(allGamePieces[column, i] == null && allTiles[column,i].tileType != TileType.Obstacle)
			 {
				 for(int j = i + 1; j < height; j++)
				 {
					 if(allGamePieces[column,j] != null)
					 {
						allGamePieces[column,j].Move(column,i,collapseTime);
						allGamePieces[column,i] = allGamePieces[column,j];
						allGamePieces[column,i].SetCoord(column,i);

						if(!movingPieces.Contains(allGamePieces[column,i]))
						{
							movingPieces.Add(allGamePieces[column,i]);
						}

						allGamePieces[column, j] = null;
						break;
					 }
				 }
			 }
		 }

		 return movingPieces;
	}

	List<GamePiece> CollapseColumn(List<GamePiece> gamePieces)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();
		List<int> columnsToCollapse = GetColumns(gamePieces);

		foreach(int column in columnsToCollapse)
		{
			movingPieces = movingPieces.Union(CollapseColumn(column)).ToList();
		}

		return movingPieces;
	}

	List<int> GetColumns(List<GamePiece> gamePieces)
	{
		List<int> columns = new List<int>();

		foreach(GamePiece piece in gamePieces)
		{
			if(!columns.Contains(piece.xIndex))
			{
				columns.Add(piece.xIndex);
			}
		}

		return columns;
	}

	void ClearAndRefillBoard(List<GamePiece> gamePieces)
	{
		StartCoroutine(ClearAndRefillBoardRoutine(gamePieces));
	}

	IEnumerator ClearAndRefillBoardRoutine(List<GamePiece> gamePieces)
	{
		playerInputEnabled = false;
		List<GamePiece> matches = gamePieces;

		//We do this check to ensure we don't populate matches when refilling
		do
		{
			yield return StartCoroutine(ClearAndCollapseRoutine(matches));
			yield return null;

			yield return StartCoroutine(RefillRoutine());
			matches = FindAllMatches();

			yield return new WaitForSeconds(0.2f);
		}

		while(matches.Count != 0);

		playerInputEnabled = true;


	}

	IEnumerator RefillRoutine()
	{
		FillBoard(fillYOffset, fillMoveTime);
		yield return null;
	}

	IEnumerator ClearAndCollapseRoutine(List<GamePiece> gamePieces)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();
		List<GamePiece> matches = new List<GamePiece>();

		//HighLightPieces(gamePieces);

		yield return new WaitForSeconds(0.5f);

		bool isFinished = false;
		while(!isFinished)
		{
			//Get those bomb pieces and the pieces they ASSPLODE and add them to our list of pieces to collapse
			List<GamePiece> bombedPieces = GetBombedPieces(gamePieces);
			gamePieces = gamePieces.Union(bombedPieces).ToList();

			ClearPieceAt(gamePieces);
			BreakTileAt(gamePieces);

			if(clickedTileBomb != null)
			{
				ActivateBomb(clickedTileBomb);
				clickedTileBomb = null;
			}
			
			if(targetTileBomb != null)
			{
				ActivateBomb(targetTileBomb);
				targetTileBomb = null;
			}

			yield return new WaitForSeconds(0.2f);

			movingPieces = CollapseColumn(gamePieces);

			while(!IsCollapsed(gamePieces))
			{
				yield return null;
			}

			//yield return new WaitForSeconds(0.5f);

			matches = FindMatchesAt(movingPieces);

			if(matches.Count == 0)
			{
				isFinished = true;
				break;
			}
			else
			{
				yield return StartCoroutine(ClearAndCollapseRoutine(matches));
			}
		}

		yield return null;
	}

	bool IsCollapsed(List<GamePiece> gamePieces)
	{
		foreach(GamePiece piece in gamePieces)
		{
			if(piece != null)
			{
				if(piece.transform.position.y - (float) piece.yIndex > 00.01f)
				{
					return false;
				}
			}
		}

		return true;
	}

	//Gets a row of gamepieces. Usually for row bombs
	List<GamePiece> GetRowPieces(int row)
	{
		List<GamePiece> gamePieces = new List<GamePiece>();
		for(int i = 0; i < width; i++)
		{
			if(allGamePieces[i,row] != null)
			{
				gamePieces.Add(allGamePieces[i,row]);
			}
		}

		return gamePieces;
	}

	List<GamePiece> GetColumnPieces(int column)
	{
		List<GamePiece> gamePieces = new List<GamePiece>();
		for(int i = 0; i < height; i++)
		{
			if(allGamePieces[column,i] != null)
			{
				gamePieces.Add(allGamePieces[column,i]);
			}
		}

		return gamePieces;
	}

	//Offset amounts determines our "radius" so to speak
	//1 means one square up, down, left and right, so 1 = 3x3 area, 2 = 5x5 area, etc.
	List<GamePiece> GetAdjacentPieces(int x, int y, int offset = 1)
	{
		List<GamePiece> gamePieces = new List<GamePiece>();
		//We get the area from the x amount and the offset, then y and offset
		for(int i = x - offset; i <= x + offset; i++)
		{
			for(int j = y - offset; j <= y + offset; j++)
			{
				if(IsWithinBounds(i,j))
				{
					gamePieces.Add(allGamePieces[i,j]);
				}
			}
		}

		return gamePieces;

	}

	List<GamePiece> GetBombedPieces(List<GamePiece> gamePieces)
	{
		//We have two lists, one for checking all the different types of bombs we have
		//The other deciding what bombs to ASSPLODE
		List<GamePiece> allPiecesToClear = new List<GamePiece>();

		foreach(GamePiece piece in gamePieces)
		{
			if(piece != null)
			{
				List<GamePiece> piecesToClear = new List<GamePiece>();
				Bomb bomb = piece.GetComponent<Bomb>();

				if(bomb != null)
				{
					switch(bomb.bombType)
					{
						case BombType.Column:
						piecesToClear = GetColumnPieces(bomb.xIndex);
						break;

						case BombType.Row:
						piecesToClear = GetRowPieces(bomb.yIndex);
						break;

						case BombType.Adjacent:
						piecesToClear = GetAdjacentPieces(bomb.xIndex, bomb.yIndex,1);
						break;

						case BombType.Color:
						Debug.Log("Color bomb found");
						break;
					}

					allPiecesToClear = allPiecesToClear.Union(piecesToClear).ToList();
				}

			}

		}

		return allPiecesToClear;
	}

	//Method checks for corner matches for adjacent bomb
	//Only works this way because of how FindMatchesAt is structured
	bool IsCornerMatch(List<GamePiece> gamePieces)
	{
		bool vertical = false;
		bool horizontal = false;
		int xStart = -1;
		int yStart = -1;

		foreach(GamePiece piece in gamePieces)
		{
			if(piece != null)
			{
				if(xStart == -1 || yStart == -1)
				{
					xStart = piece.xIndex;
					yStart = piece.yIndex;
					continue;
				}

				if(piece.xIndex != xStart && piece.yIndex == yStart)
				{
					horizontal = true;
				}

				if(piece.xIndex == xStart && piece.yIndex != yStart)
				{
					vertical = true;
				}
			}
		}

		return(horizontal && vertical);
	}

	GameObject DropBomb(int x, int y, Vector2 swapDirection, List<GamePiece> gamePieces)
	{
		GameObject bomb = null;

		if(gamePieces.Count >= 4)
		{
			if(IsCornerMatch(gamePieces))
			{
				if(adjacentBombPrefab != null)
				{
					bomb = MakeBomb(adjacentBombPrefab, x, y);
				}
			}
			else
			{
				//Row Bomb
				if(swapDirection.x != 0)
				{
					if(rowBombPrefab != null)
					{
						bomb = MakeBomb(rowBombPrefab, x, y);
					}
				}
				//Columb Bomb
				else
				{
					if(columnBombPrefab != null)
					{
						bomb = MakeBomb(columnBombPrefab, x, y);
					}
				}
			}
		}

		return bomb;
	}

	//Bombs can't be added to the allgamepieces array, so we add it temporarily with this call then remove it
	void ActivateBomb(GameObject bomb)
	{
		int x = (int) bomb.transform.position.x;
		int y = (int) bomb.transform.position.y;

		if(IsWithinBounds(x,y))
		{
			allGamePieces[x,y] = bomb.GetComponent<GamePiece>();
		}
	}
    
}